export default function goToCheckout(e) {
  if(this.cartSize === 0) {
    e.preventDefault()
    e.stopPropagation()
    return
  }

  this.goingToCheckout = true
  setTimeout(() => {
    this.goingToCheckout = false
  }, 5000)
}