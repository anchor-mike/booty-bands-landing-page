export default {
  namespaced: true,

  state: {
    cartId: '',
    cart: {},
    addingToCart: false,
    updatingCart: false,
    emptyingCart: false,
    estimatingShipping: false,
    estimatedShipping: {},
    selectedShippingRate: {},
  },

  mutations: {
    setCartId (state, sentId) {
      state.cartId = sentId
    },

    setCart (state, sentCart) {
      state.cart = JSON.parse(JSON.stringify(sentCart))
    },

    setEstimatedShipping(state, sentEstimatedShipping) {
      state.estimatedShipping = sentEstimatedShipping
    },

    toggleAddingToCart (state) {
      state.addingToCart = !state.addingToCart
    },

    toggleUpdatingCart (state) {
      state.updatingCart = !state.updatingCart
    },

    toggleEmptyingCart (state) {
      state.emptyingCart = !state.emptyingCart
    },

    toggleEstimatingShipping (state, toggle = null) {
      state.estimatingShipping = toggle || !state.estimatingShipping
    },

    setSelectedShippingRate (state, sentSelectedShippingRate) {
      state.selectedShippingRate = JSON.parse(JSON.stringify(sentSelectedShippingRate))
    }
  },

  actions: {
    async createCart (store) {
      const cart = await store.rootState.client.checkout.create()

      localStorage.setItem('cartId', cart.id)
      store.commit('setCartId', cart.id)
      store.commit('setCart', cart)
    },

    // reset when storage is more than X hours
    setLSTime () {
      const hours = 24
      const now = new Date().getTime()
      const setupTime = localStorage.getItem('setupTime')
      if (setupTime == null) {
        localStorage.setItem('setupTime', now)
      } else {
        if (now - setupTime > hours * 60 * 60 * 1000) {
          localStorage.removeItem('cartId')
          localStorage.setItem('setupTime', now)
        }
      }
    },

    async fetchCart (store, dontEstimateShipping) {
      await store.rootState.client.checkout.fetch(store.state.cartId).then((cart) => {
        store.commit('setCart', cart)
        if(!dontEstimateShipping && cart.estimateShipping) {
          store.dispatch('estimateShipping')
        }
      }).catch(() => {
        // Reset LS
        localStorage.removeItem('cartId')
        store.dispatch('setLSTime')

        // refetch cart
        store.dispatch('createCart')
      })
    },

    async addProduct (store, sentProductData) {
      store.commit('toggleAddingToCart')
      const cart = await store.rootState.client.checkout.addLineItems(store.state.cartId, sentProductData)
      store.commit('setCart', cart)
      store.commit('toggleAddingToCart')
    },

    async addMultipleProducts (store, sentVariants) {
      let lineItems = []
      sentVariants.forEach(async sentVariant => {
        lineItems.push({
          variantId: sentVariant,
          quantity: 1
        })
      })

      store.commit('toggleAddingToCart')
      const cart = await store.rootState.client.checkout.addLineItems(store.state.cartId, lineItems)
      store.commit('setCart', cart)
      store.commit('toggleAddingToCart')
    },

    async updateProduct (store, sentProductData) {
      store.commit('toggleUpdatingCart')
      const cart = await store.rootState.client.checkout.updateLineItems(store.state.cartId, sentProductData)
      store.commit('setCart', cart)
      store.commit('toggleUpdatingCart')
    },

    async removeProduct (store, sentProductData) {
      store.commit('toggleUpdatingCart')
      const cart = await store.rootState.client.checkout.removeLineItems(store.state.cartId, sentProductData)
      store.commit('setCart', cart)
      store.commit('toggleUpdatingCart')
    },

    async emptyCart (store) {
      store.commit('toggleEmptyingCart')
      store.commit('toggleUpdatingCart')
      let lineItemIdsToRemove = []
      store.state.cart.lineItems.forEach(item => lineItemIdsToRemove.push(item.id))

      const cart = await store.rootState.client.checkout.removeLineItems(store.state.cartId, lineItemIdsToRemove)
      store.commit('setCart', cart)
      store.commit('toggleEmptyingCart')
      store.commit('toggleUpdatingCart')
    },

    async applyDiscount (store, discountCode) {
      const cart = await store.rootState.client.checkout.addDiscount(store.state.cartId, discountCode)
      store.commit('setCart', cart)
    },

    async removeDiscount (store, discountCode) {
      const cart = await store.rootState.client.checkout.removeDiscount(store.state.cartId)
      store.commit('setCart', cart)
    },

    async addAddressInfoAndEstimateShipping (store, addressInfo) {
      store.commit('toggleEstimatingShipping')
      await store.rootState.client.checkout.updateShippingAddress(store.state.cartId, addressInfo)
      await store.dispatch('estimateShipping')
    },

    async estimateShipping (store) {
      store.commit('toggleEstimatingShipping', true)

      const estimatedShippingQuery = await store.rootState.client.graphQLClient.query((root) => {
        root.add('node', { args: { id: store.state.cartId } }, (node) => {
          node.add('id');
          node.addInlineFragmentOn('Checkout', (checkout) => {
            checkout.add('totalTax')
            checkout.add('taxesIncluded')
            checkout.add('taxExempt')
            checkout.add('subtotalPrice')
            checkout.add('totalPrice')
            checkout.add('email')
            checkout.add('createdAt')
            checkout.add('webUrl')
            checkout.add('requiresShipping')
            checkout.add('availableShippingRates', (rates) => {
              rates.add('ready')
              rates.add('shippingRates', (rate) => {
                rate.add('title')
                rate.add('price')
                rate.add('handle')
              })
            })
          })
        })
      })

      store.rootState.client.graphQLClient.send(estimatedShippingQuery).then(({model}) => {
        const transformedData = JSON.parse(JSON.stringify(model))
        if(
          transformedData.node 
          && transformedData.node.availableShippingRates 
        ) {
          // Per: https://shopify.dev/docs/storefront-api/reference/checkouts/availableshippingrates
          // The shippingRates field is null when this value is false. 
          // This field should be polled until its value becomes true.
          if(transformedData.node.availableShippingRates.ready === false) {
            store.dispatch('estimateShipping')
          } else if(transformedData.node.availableShippingRates.shippingRates) {
            store.commit('setEstimatedShipping', transformedData.node.availableShippingRates.shippingRates)
            store.dispatch('fetchCart', true) // refetch cart after the fact
          }
        }

        if(store.state.estimatingShipping) {
          store.commit('toggleEstimatingShipping', false)
        }
      })
    },

    async applyGiftCard (store, giftCardCode) {
      const giftCardCart = await store.rootState.client.checkout.addGiftCards(store.state.cartId, giftCardCode)
      store.commit('setCart', giftCardCart)
    },

    async removeGiftCard (store, giftCardId) {
      const giftCardCart = await store.rootState.client.checkout.removeGiftCard(store.state.cartId, giftCardId)
      store.commit('setCart', giftCardCart)
    },
  },

  getters: {
    cartSize (state) {
      let cartAmount = 0
      if (state.cart && state.cart.lineItems) {
        state.cart.lineItems.forEach(item => { cartAmount += item.quantity })
      }

      return cartAmount
    },

    cartDiscounts(state) {
      return state.cart && state.cart.discountApplications || []
    },
  }
}
