import { createStore } from 'vuex'
import Client from 'shopify-buy/index.unoptimized.umd'

import cart from './modules/cart'
import products from './modules/products'
import store from './modules/store'

// ------------------
// SHOPIFY BUY SDK CLIENT
// ------------------
const client = Client.buildClient({
  domain: import.meta.env.VITE_domain,
  storefrontAccessToken: import.meta.env.VITE_storefrontAccessToken
})

if (import.meta.MODE !== 'production') {
  console.log(client) //eslint-disable-line
}

export default createStore({
  state: {
    client,
    showSideCart: false,
    pageYOffset: 0
  },

  mutations: {
    toggleSideCart (state) {
      state.showSideCart = !state.showSideCart

      if (state.showSideCart) {
        state.pageYOffset = window.pageYOffset
        document.body.classList.add('no-scroll')

        // for ios safari
        if (navigator.platform.match(/iPhone/)) {
          window.scrollTo(0, -100)
        }

        if (window.scrollY) {
          document.body.classList.add('sticky-active')
        }
      } else {
        window.scrollTo(0, state.pageYOffset)
        document.body.classList.remove('sticky-active')
        document.body.classList.remove('no-scroll')
      }
    }
  },

  modules: {
    store,
    cart,
    products
  },
})
