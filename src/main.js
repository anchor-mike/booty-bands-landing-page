import { createApp } from 'vue'
import { createRouter, createWebHistory } from 'vue-router'
import App from './App.vue'
import { setMeta } from './meta/setMeta'
import vuexStore from './store/index'
import VueImage from './components/VueImage.vue'
import Loading from './components/Loading.vue'
import QuantityInput from './components/QuantityInput.vue'
import ProductListItem from './components/ProductListItem.vue'
import ImageSlider from './components/ImageSlider.vue'

let availableRoutes = [
  { path: '/products', name: 'Products' },
  { path: '/products/booty-bands-barbells', name: 'Barbell' },
  { path: '/style-guide', name: 'Style Guide' },
  // { path: '/store', name: 'Store' },
  // { path: '/search', name: 'Search' },
  // { path: '/collection', name: 'Collection' },
  // { path: '/collection/lps', name: 'LPs' },
  // { path: '/product', name: 'Product' },
  // { path: '/product/kendrick-lamar-damn-lp"', name: 'Damn 2xLP' },
]

const router = createRouter({
  history: createWebHistory(),
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { top: 0 }
    }
  },
  routes: [
    { path: '/products/booty-bands-barbells', name: 'Barbell', component: () => import('./views/Barbell.vue') },
    { path: '/style-guide', name: 'Style Guide', component: () => import('./views/StyleGuide.vue') },
    // { path: '/about', name: 'About', component: () => import('./views/About.vue') },
    // { path: '/contact', name: 'Contact', component: () => import('./views/Contact.vue') },
    // { path: '/store', name: 'Store', component: () => import('./views/Products.vue') },
    // { path: '/collection/:id', name: 'Collection', component: () => import('./views/Products.vue') },
    // { path: '/search/:id', name: 'Search', component: () => import('./views/Search.vue'), alias: '/search' },
    // { path: '/product/:id', name: 'Product', component: () => import('./views/Product.vue') },
    // { path: '/product/bundle', name: 'Bundle Product', component: () => import('./views/BundleProduct.vue') },
    // { path: '/cart', name: 'Cart', component: () => import('./views/Cart.vue') },
  ],
})
router.beforeEach((guard) => {
  setMeta(guard)
})

const app = createApp(App)

// app.use(router)
app.use(vuexStore)

app.component('vue-image', VueImage)
app.component('loading', Loading)
app.component('quantity-input', QuantityInput)
app.component('product-list-item', ProductListItem)
app.component('image-slider', ImageSlider)

// Vue 3.0 filters
app.config.globalProperties.$filters = {
  currency(n) {
    return parseFloat(n).toLocaleString('en-US', {
      style: 'currency',
      currency: 'USD'
    }).replace('.00', '')
  }
}

app.mount('#app')
