export default function baseMeta () {
  let baseMeta = {
    title: 'Booty Bands',
    description: 'More than just Bands! Booty Bands® can Lift, Round, Firm & Shape your Booty. Adding Booty Bands® to core & cardio routines can shrink your waist & lose unwanted inches. Booty Bands® is the best solution to your NEW perfect shape! Our products & community focus on self-love first, working from the inside out.',
    keywords: 'booty, bands',
    image: '/android-chrome-512x512.png'
  }

  if (window.storeData) baseMeta = { ...baseMeta, ...window.storeData }

  return baseMeta
}
