import baseMeta from './baseMeta'
import setMetaHelper from './setMetaHelper'

export function setMeta (to) {
  let meta = { ...baseMeta(), ...to }
  if (window.storeData) meta = JSON.parse(JSON.stringify(window.storeData))
  meta.title = `${to.name} | ${meta.title}`

  setMetaHelper(meta)
}

// export function setMeta (to, extraMeta) {
//   let meta = { ...baseMeta(), ...extraMeta }
//   if (window.storeData) meta = JSON.parse(JSON.stringify(window.storeData))

//   if (to.name === 'home-page') {
//     meta.title = `Electric Eye Increases Sales For Ecommerce Brands | ${meta.title}`
//   } else if (to.name === 'page' || to.name === 'pages' || to.name === 'collection' || to.name === 'product') {
//     meta.title = `${meta.newTitle || to.params.uid.replace(/-/g, ' ').replace(/\b\w/g, l => l.toUpperCase())} | ${meta.title}`
//   }

//   setMetaHelper(meta)
// }
