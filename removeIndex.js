const fs = require('fs')
const path = require('path')

// delete index.html
fs.unlinkSync(path.join(__dirname, '/dist/index.html'))
console.log('💣 removed index.html')

fs.unlinkSync(path.join(__dirname, '/dist/products/index.html'))
console.log('💣 removed Products index.html')
