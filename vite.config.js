import path from 'path'
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
// import { VitePWA } from 'vite-plugin-pwa'
import prerenderSpaPlugin from 'rollup-plugin-prerender-spa-plugin'
// import SitemapPlugin from 'rollup-plugin-sitemap'

const availableRoutes = [
  { path: '/products', name: 'Products' },
  { path: '/products/booty-bands-barbells', name: 'Barbell' }
]

// https://vitejs.dev/config/
export default defineConfig({
  server: {
    port: 6969
  },
  plugins: [
    vue(), 
    prerenderSpaPlugin({
      staticDir: path.resolve(__dirname, 'dist'),
      routes: availableRoutes.map(route => route.path),
      // https://github.com/chrisvfritz/prerender-spa-plugin
      // https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#puppeteerlaunchoptions
      puppeteer: {
        renderAfterDocumentEvent: 'x-app-rendered',
        injectProperty: 'prerenderSettings',
        inject: {
          prerendered: true
        }
      }
    }),
    // SitemapPlugin({
    //   baseUrl: 'https://warroad.netlify.app',
    //   contentBase: 'dist',
    //   routes: availableRoutes,
    // }),
    // VitePWA({
    //   strategies: 'GenerateSW',
    //   manifest: {
    //     name: 'BootyBands Barbell',
    //     themeColor: '#ff3399',
    //     msTileColor: '#ff3399',
    //     theme_color: '#ff3399',
    //     appleMobileWebAppCapable: 'yes',
    //     appleMobileWebAppStatusBarStyle: 'black',
    //     icons: [
    //       {
    //         src: '/favicon-16x16.png',
    //         sizes: '16x16',
    //         type: 'image/png'
    //       },
    //       {
    //         src: '/favicon-32x32.png',
    //         sizes: '32x32',
    //         type: 'image/png'
    //       },
    //       {
    //         src: '/app-icon-150x150.png',
    //         sizes: '150x150',
    //         type: 'image/png'
    //       },
    //       {
    //         src: '/app-icon-144x144.png',
    //         sizes: '144x144',
    //         type: 'image/png'
    //       },
    //       {
    //         src: '/android-chrome-192x192.png',
    //         sizes: '192x192',
    //         type: 'image/png',
    //         purpose: 'any maskable'
    //       },
    //       {
    //         src: '/android-chrome-512x512.png',
    //         sizes: '512x512',
    //         type: 'image/png'
    //       }
    //     ],
    //     workboxOptions: {
    //       exclude: [/\.map$/, /_redirects/]
    //     }
    //   }
    // })
  ],
  resolve: {
    alias: [
      {
        find: '@',
        replacement: path.resolve(__dirname, './src')
      }
    ]
  },
  css: {
    preprocessorOptions: {
      scss: { 
        additionalData: `@use "sass:math"; @import "@/styles/_variables.scss";`
      }
    }
  },
  // assetsInclude: ['sourcemap.xml'] //have to do this for sourcemap to copy over
})
