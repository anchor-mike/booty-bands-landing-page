const fetch = require('node-fetch')
global.fetch = fetch

const fs = require('fs')
const path = require('path')
require('dotenv').config()

const Client = require('shopify-buy')

exports.generateRoutes = function() {
  let availableRoutes = [
    { path: '/products', name: 'Products' },
    { path: '/products/booty-bands-barbells', name: 'Barbell' },
    // { path: '/about', name: 'About' },
    // { path: '/contact', name: 'Contact' },
    { path: '/style-guide', name: 'Style Guide' },
    // { path: '/store', name: 'Store' },
    // { path: '/search', name: 'Search' },
    // { path: '/collection', name: 'Collection' },
    // { path: '/collection/lps', name: 'LPs' },
    // { path: '/product', name: 'Product' },
    // { path: '/product/kendrick-lamar-damn-lp', name: 'Damn 2xLP' }
  ]


  // pull products from storefront api
  async function getProducts() {
    const client = Client.buildClient({
      domain: process.env.VITE_domain,
      storefrontAccessToken: process.env.VITE_storefrontAccessToken
    })
    const productsQuery = await client.graphQLClient.query((root) => {
      root.addConnection('products', { args: { first: 250 } }, (product) => {
        product.add('handle')
        product.add('title')
      })
    })

    console.log('🚀 Generating Routes')

    await client.graphQLClient.send(productsQuery).then(({model}) => {
      if(model && model.products) {
        model.products.forEach((product) => {
          availableRoutes.push({
            path: `/product/${product.handle}`,
            name: product.title
          })
        })
      }
    }).catch(err => console.log(err))

    console.log(`💰 We have ${availableRoutes.length} routes`)
    console.log(`💾 Saving`)

    fs.writeFile('routes.js', 'export default ' + JSON.stringify(availableRoutes), function (err) {
      if (err) return console.log(err);
      console.log(`✨ ${availableRoutes.length} routes saved to routes.json`)
    })

    return availableRoutes
  }

  return getProducts()
}

module.exports = exports